<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="js/app.js">
    <title>CRUD Met PHP</title>
  </head>
  <body class="Updatebod">

  <!--telefoon navbar hier-->
<div class="topnav" id="myTopnav">
    <a href="Homepage.html">Homepage</a>
    <a class="active" href="Scoreboard.php">Scoreboard</a>
    <a href="contact.html">Contact</a>
    <a href="overons.html">Over ons</a>
    <a href="esc.html">Escape rooms</a>
    <a href="javascript:void(0);" class="icon" onclick="myFunction()">
    <i class="fa fa-bars">☰</i>
  </a>
</div>

<!--normale navbar hier-->
<ul id="normalnavbar">
    <li><a href="Homepage.html">Homepage</a></li>
    <li><a class="active" href="Scoreboard.php">Scoreboard</a></li>
    <li><a href="contact.html">Contact</a></li>
    <li><a href="overons.html">Over ons</a></li>
    <li><a href="esc.html">Escape rooms</a></li>
</ul>
  <?php

// hier worden inlog gegevens ingevoerd voor de database
$dbserver = "localhost";
$dbuser = "root";
$dbpassword = "";
$dbname = "scoreboard";

//verbind met de database
$conn = mysqli_connect ($dbserver, $dbuser, $dbpassword, $dbname);

$id = $_GET["id"];

$sql = "SELECT * FROM `scores` WHERE ID = $id";

$result = mysqli_query($conn, $sql);

$record = mysqli_fetch_assoc($result);


?>

<div class="container-fluid" id="banner" >
<h1>Bewerk je scores</h1>
</div>

<div class="edits">

<div class="container">
<div class="row">

<div class="mb-3">
<form action="./updatescript.php" method="post">
<label for="Naam">Naam</label>
<br>
<input value="<?php echo $record["Naam"]; ?>" placeholder="(vereist)" type="text" name="Naam" id="Naam" Required>
</div>

<div class="mb-3">
<label for="Email">Email</label>
<br>
<input value="<?php echo $record["Email"]; ?>" placeholder=""type="text" name="Email" id="Email">
</div>
                
<div class="mb-3">
<label for="Tijd">Tijd</label>
<br>
<input value="<?php echo $record["Tijd"]; ?>" type="text" name="Tijd" id="Tijd">
</div> 

<div class="mb-3">
<label for="bericht">Bericht</label>
<br>
<input value="<?php echo $record["bericht"]; ?>" placeholder=""type="text" name="bericht" id="bericht">
</div>

<div class="mb-3">
<label for="Tevreden">Tevredenheid</label>
<br>
<input value="<?php echo $record["Tevreden"]; ?>" placeholder=""type="text" name="Tevreden" id="Tevreden">
</div>

</div>

<br>

<div class="">
<input type="hidden" name="id" value="<?php echo $id; ?>">
<input class="Confirmbtn" type="submit" value="verander">
<a class="" href="updatescript.php" role="button"></a>
</div>

</form>

<br>

<form action="./deletescript.php" method="post">
            
<div class="">
<input type="hidden" name="id" value="<?php echo $id; ?>">
<input class="Deletebtn" type="submit" value="verwijder">
<a class="" href="deletescript.php" role="button"></a>
</div>

</form>
    
<br>

<a class="Backbtn" href="./Scoreboard.php">Anuleren</a>

</form>



</div>
</div>
</div>

    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>


    <script>
    function myFunction() {
      var x = document.getElementById("myTopnav");
      if (x.className === "topnav") {
        x.className += " responsive";
      } else {
        x.className = "topnav";
      }
    }
    </script>
</body>
</html>

