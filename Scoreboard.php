<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="style.css">
    <link rel="shortcut icon" href="favicon.ico">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Comic+Neue:wght@300&family=Roboto&display=swap" rel="stylesheet">
    
    <title>Homepage</title>
</head>
<body>

    <!--telefoon navbar hier-->
<div class="topnav" id="myTopnav">
    <a href="Homepage.html">Homepage</a>
    <a class="active" href="Scoreboard.php">Scoreboard</a>
    <a href="contact.html">Contact</a>
    <a href="overons.html">Over ons</a>
    <a href="esc.html">Escape rooms</a>
    <a href="javascript:void(0);" class="icon" onclick="myFunction()">
    <i class="fa fa-bars">☰</i>
  </a>
</div>

<!--normale navbar hier-->
<ul id="normalnavbar">
    <li><a href="Homepage.html">Homepage</a></li>
    <li><a class="active" href="Scoreboard.php">Scoreboard</a></li>
    <li><a href="contact.html">Contact</a></li>
    <li><a href="overons.html">Over ons</a></li>
    <li><a href="esc.html">Escape rooms</a></li>
</ul>

<?php

// hier worden inlog gegevens ingevoerd voor de database
$dbserver = "localhost";
$dbuser = "root";
$dbpassword = "";
$dbname = "scoreboard";

//verbind met de database
$conn = mysqli_connect ($dbserver, $dbuser, $dbpassword, $dbname);

$sql = 'select * from scores';
$query = mysqli_query($conn, $sql);

if(!$query)
{
  die(mysqli_connect_error());
}

$sql = 'select * from scores';
$query = mysqli_query($conn,$sql);

if(!$query)

{
  die('error found' . mysqli_error($conn));
}

?>

<table>
  <tr>
    <th>ID</th>
    <th>Naam</th>
    <th>E-mail</th>
    <th>Tijdscore</th>
    <th>Bericht</th>
    <th>Rating</th>
    <th>Bewerken</th>
  </tr>

<?php

  while ($row = mysqli_fetch_array($query)) 
  {
    echo ' <tr> 
    <td>'.$row['ID'].'</td>
    <td>'.$row['Naam'].'</td>
    <td>'.$row['Email'].'</td>
    <td>'.$row['Tijd'].'</td>
    <td>'.$row['bericht'].'</td>
    <td>'.$row['Tevreden'].'</td>

    <td> <a href="update.php?id='.$row['ID'].'"> <img src="./Pictures/pencil.png" alt="pen" width="30vw"> <i class="bi bi-wrench"> </i> </a> </td>

    </tr>';
  }

  echo "</table>";
  
?>
  
<!--hier kan je gegevens invoeren-->
<div class="input">
  <form action="create.php" method="post">
  <label for="Naam">Je naam</label>
  <br>
  <input placeholder="" type="text" name="Naam" id="Naam" Required>
  <br>
  <form action="create.php" method="post">
  <label for="Email">Je E-Mail adress</label>
  <br>
  <input placeholder="" type="text" name="Email" id="Email" Required>
  <br>
  <form action="create.php" method="post">
  <label for="Tijd">Je tijdscore</label>
  <br>
  <input placeholder="" type="text" name="Tijd" id="Tijd" Required>
  <br>
  <form action="create.php" method="post">
  <label for="Bericht">Laat een bericht achter</label>
  <br>
  <input placeholder="(optioneel)" type="text" name="Bericht" id="Bericht">
  <br>
  <label for="Tevreden">Score van 1-10</label>
  <br>
  <input placeholder="" type="text" name="Tevreden" id="Tevreden" Required>
  
  <input placeholder ehehehe type="submit" value="SUBMIT">
</div>



<script>
    function myFunction() {
      var x = document.getElementById("myTopnav");
      if (x.className === "topnav") {
        x.className += " responsive";
      } else {
        x.className = "topnav";
      }
    }
    </script>
<br>
</body>
</html>

