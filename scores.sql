-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Gegenereerd op: 08 dec 2021 om 15:26
-- Serverversie: 5.7.31
-- PHP-versie: 7.3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `scoreboard`
--

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `scores`
--

DROP TABLE IF EXISTS `scores`;
CREATE TABLE IF NOT EXISTS `scores` (
  `ID` int(2) NOT NULL AUTO_INCREMENT,
  `Naam` varchar(300) NOT NULL,
  `Email` varchar(50) NOT NULL,
  `Tijd` time NOT NULL,
  `bericht` varchar(1000) DEFAULT NULL,
  `Tevreden` int(10) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ID` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `scores`
--

INSERT INTO `scores` (`ID`, `Naam`, `Email`, `Tijd`, `bericht`, `Tevreden`) VALUES
(15, 'Jesuzz', 'Jchrist@holy.com', '20:21:00', 'ayo its me jesus', 10),
(18, 'geit', 'baa@baa.baa', '00:05:11', 'baa', 10),
(20, 'Obama Bin Laden Barack Obama Hussein', 'Obama@whitehouse.com', '08:00:00', 'hello. its me. president obama bin ladin barack obama. i love escape rooms. goodbye fellow americans', 8),
(21, ' Haritha Computers & Technology ', 'Haritha@gmail.com', '11:44:00', 'PHP | How To Display MySQL (phpMyAdmin 4.5.1) Data Into HTML Table', 10),
(24, 'lammetje lilililili', 'bahh', '00:15:00', 'baahh', 5);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
